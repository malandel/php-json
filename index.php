<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./node_modules/semantic-ui-card/card.css">
    <title>Computer Science Figures</title>
</head>

<body>
    <h1>Computer Science Figures</h1>
    <!--Ouvre la div dans laquelle seront contenues toutes les cartes créées en PHP, CSS class Semantic UI -->
    <div class="ui fluid cards">
        <?php
        //Lire le fichier CSV, le convertir en array 
        function read($csv)
        {
            $bio = fopen($csv, 'r');
            while (!feof($bio)) {
                $line[] = fgetcsv($bio);
            }
            fclose($bio);
            return $line;
        }
        // Définir le chemin d'accès au fichier CSV
        $csv = 'historicFigures.csv';
        //Appeler la fonction pour le convertir en array multidimensionnel (?)
        $csv = read($csv);
        //Lire l'array, générer une carte HTML à chaque index (correspond à une bio)
        foreach ($csv as $personne) {
            //Exclure le premier index car pas de contenu intéressant, exlure les contenus vides
            if ($i++ < 1 || empty($personne)) {
                continue;
            }
        ?>
        <div class="ui card">
            <div class="image">
                <img src="<?= $personne[5]?>">
            </div>
            <div class="content">
                <div class="header">
                    <?= $personne[0] ?>
                </div>
                <div class="meta">
                    <?= $personne[2] ?>
                </div>
                <div class="description">
                    <?= $personne[3] ?>
                </div>
            </div>
            <div class="extra content">
                <span class="right floated">
                    Born in <?= $personne[1] ?>
                </span>
            </div>
        </div>

        <?php
            //Ne pas oublier de fermer la boucle foreach
        }
        ?>
    <!--Fermer la div générale ouverte avant la procédure php -->
    </div>
</body>

</html>